/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructure.st1207;

import mx.edu.utr.datastructures.List;

public class ArrayList implements List {

    private Object[] elements;
    private int size;

    @Override
    public boolean add(Object o) {
        ensureCapacity(size + 1);
        elements[size++] = o;
        return true;
    }

    @Override
    public void add(int index, Object element) {
    
        rangeCheck(index);
        ensureCapacity(size + 1);
        elements[size++] = element;
        size++;
        

    }

    @Override
    public void clear() {
        for (int i = 0; i < elements.length; i++) {
            elements[i] = 0;
        }
    }

    @Override
    public Object get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return elements[index];
    }

    @Override
    public int indexOf(Object o) {

        for (int i = 0; i < elements.length; i++) {

            if (elements[i] == o) {
                System.out.println(i);
            }
        }

        return -1;

    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Object remove(int index) {
        Object oldElement = elements[index];
        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;

    }

    @Override
    public Object set(int index, Object element) {

        if (index >= size) {
            throw new IndexOutOfBoundsException();
        } else {
            Object old = elements[index];
            elements[index] = elements;
            return old;
        }
    }

    @Override
    public int size() {
        return size;
    }
    
    //increases the capacity of the Array

    private void ensureCapacity(int i) {
        int oldCapacity = elements.length;
        if (i > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            Object[] nelements;
            nelements = new Object[newCapacity];
            for (int x = 0; x <= oldCapacity; x++) {
                nelements[x] = elements[x];
            }
        }
    }

    //
    
    private void rangeCheck(int index) {
        if(index < 0 || index > size)
        {
            throw new IndexOutOfBoundsException();
        }
    }

}
